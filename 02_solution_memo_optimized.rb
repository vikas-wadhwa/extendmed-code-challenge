##########################################################################################################################
##  Extendmed Coding Solutions - 02 - Memo optimized
##
##  Created by Vikas Wadhwa  https://bitbucket.org/vikas-wadhwa
##########################################################################################################################
# As a first step to optimize the brute force solution, we create a @memo hash variable
# and populate it with the first known value, 1 => 1
# We then instead reverse the search, going from 1 upwards, in order to save the term counts as we go along.
# Any subsequent starting integer which has a known integer in it's sequence will then pull from the hash,
# rather than repeating the recursion again.

def solve(n)
  # create a memoization hash, to store results 
  # of a recursive sequence when we've done it before
  @memo = { 1 => 1 }
  start = Time.now
  result = 1
  max_terms = 1

  (1..n).each do |n|
    # if the starting value already had terms calculated, pull it from the memo
    terms = @memo[n] || recurse_sequence(n, 0)

    # if the memo value doesn't exist, populate it with the term count calculated
    @memo[n] ||= terms
    if terms > max_terms
      max_terms = terms
      result = n
    end
  end
  
  puts "Calculation Time        : #{Time.now - start} seconds"
  puts "Resulting Start Integer : #{result}"
  puts "Terms                   : #{max_terms}"
  result
end

def recurse_sequence(n, terms)  
  terms += 1

  if n == 1
    terms
  elsif n.odd?
    recurse_sequence(3*n + 1, terms)
  else
    recurse_sequence(n/2, terms)
  end
end

# Test known solutions
def run_tests
  puts '------------------------------------------------------'
  puts "solve(1)  should equal 1 -> #{solve(1) == 1}"
  puts '------------------------------------------------------'
  puts "solve(2)  should equal 2 -> #{solve(2) == 2}"
  puts '------------------------------------------------------'
  puts "solve(10) should equal 9 -> #{solve(10) == 9}"
  puts '------------------------------------------------------'

  puts '------------------------------------------------------'
  puts "recurse_sequence(1, 0)  should equal 1 -> #{recurse_sequence(1, 0) == 1}"
  puts '------------------------------------------------------'
  puts "recurse_sequence(2, 0)  should equal 2 -> #{recurse_sequence(2, 0) == 2}"
  puts '------------------------------------------------------'
  puts "recurse_sequence(10, 0) should equal 7 -> #{recurse_sequence(10, 0) == 7}"
  puts '------------------------------------------------------'
end

# Run the tests, send results to console output
run_tests

# Test with starting value 999,999
solve(999999)