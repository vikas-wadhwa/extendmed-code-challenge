##########################################################################################################################
##  Extendmed Coding Solutions - 01 - Brute Force
##
##  Created by Vikas Wadhwa  https://bitbucket.org/vikas-wadhwa
##########################################################################################################################
# As a brute force solution, we are simply starting with the highest number, 999999, 
# finding out how many terms it has via a recursion that uses the sequence rules,
# and whenever we find a new starting number with a term count higher 
# than the thusfar calculated max, we update the max.
# We save the time data and the starting integer/term count as output. 

def solve(n)
  start = Time.now
  result = 1
  max_terms = 1

  n.downto(1) do |n|
    terms = recurse_sequence(n, 0)
    if terms > max_terms
      max_terms = terms
      result = n
    end
  end
  
  puts "Calculation Time        : #{Time.now - start} seconds"
  puts "Resulting Start Integer : #{result}"
  puts "Terms                   : #{max_terms}"
  result
end

def recurse_sequence(n, terms)
  terms += 1

  if n == 1
    terms
  elsif n.odd?
    recurse_sequence(3*n + 1, terms)
  else
    recurse_sequence(n/2, terms)
  end
end

# Test known solutions
def run_tests
  puts '------------------------------------------------------'
  puts "solve(1)  should equal 1 -> #{solve(1) == 1}"
  puts '------------------------------------------------------'
  puts "solve(2)  should equal 2 -> #{solve(2) == 2}"
  puts '------------------------------------------------------'
  puts "solve(10) should equal 9 -> #{solve(10) == 9}"
  puts '------------------------------------------------------'

  puts '------------------------------------------------------'
  puts "recurse_sequence(1, 0)  should equal 1 -> #{recurse_sequence(1, 0) == 1}"
  puts '------------------------------------------------------'
  puts "recurse_sequence(2, 0)  should equal 2 -> #{recurse_sequence(2, 0) == 2}"
  puts '------------------------------------------------------'
  puts "recurse_sequence(10, 0) should equal 7 -> #{recurse_sequence(10, 0) == 7}"
  puts '------------------------------------------------------'
end

# Run the tests, send results to console output
run_tests

# Test with starting value 999,999
solve(999999)