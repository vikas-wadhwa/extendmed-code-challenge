# CODE CHALLENGE 
The following sequence is defined for the set of positive integers:

n → n/2 (when n is even)
n → 3n + 1 (when n is odd)

Using the rules above and starting with 10, we generate the following sequence:

10 → 5 → 16 → 8 → 4 → 2 → 1

This sequence (starting at 10 and finishing at 1) contains 7 terms. 

Write a program to determine which starting number, under one million, produces the longest chain of terms, ending at 1.

NOTE: Once the chain starts, the terms are allowed to go above one million.

Please provide a brute force approach to solving this problem.  Once you have completed such, explain what you would do to improve the efficiency of your code.  If you wish, you may also submit code showing these improvements, but we still require written English explanation of the changes.