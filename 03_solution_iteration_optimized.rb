##########################################################################################################################
##  Extendmed Coding Solutions - 03 - Iteration optimized
##
##  Created by Vikas Wadhwa  https://bitbucket.org/vikas-wadhwa
##########################################################################################################################
# As another optimization on top of the memoization in 02, we can change the recursion to an iteration,
# since there is a loop performance degredation when going from a simple loop to a recursion

def solve(n)
  # create a memoization hash, to store results 
  # of a recursive sequence when we've done it before
  @memo = { 1 => 1 }
  start = Time.now
  result = 1
  max_terms = 1

  (1..n).each do |n|
    # if the starting value already had terms calculated, pull it from the memo
    terms = @memo[n] || count_sequence_terms(n)

    # if the memo value doesn't exist, populate it with the term count calculated
    @memo[n] ||= terms
    if terms > max_terms
      max_terms = terms
      result = n
    end
  end
  
  puts "Calculation Time        : #{Time.now - start} seconds"
  puts "Resulting Start Integer : #{result}"
  puts "Terms                   : #{max_terms}"
  result
end

def count_sequence_terms(n)  
  x = n

  # Start with 1 term since the loop does not count the final term where n == 1
  terms = 1

  until x == 1 do
    terms += 1

    if x.odd?
      x = 3*x + 1
    else
      x = x/2
    end
  end

  terms
end

def run_tests
  puts '------------------------------------------------------'
  puts "solve(1)  should equal 1 -> #{solve(1) == 1}"
  puts '------------------------------------------------------'
  puts "solve(2)  should equal 2 -> #{solve(2) == 2}"
  puts '------------------------------------------------------'
  puts "solve(10) should equal 9 -> #{solve(10) == 9}"
  puts '------------------------------------------------------'

  puts '------------------------------------------------------'
  puts "count_sequence_terms(1)  should equal 1 -> #{count_sequence_terms(1) == 1}"
  puts '------------------------------------------------------'
  puts "count_sequence_terms(2)  should equal 2 -> #{count_sequence_terms(2) == 2}"
  puts '------------------------------------------------------'
  puts "count_sequence_terms(10) should equal 7 -> #{count_sequence_terms(10) == 7}"
  puts '------------------------------------------------------'
end

# Run the tests, send results to console output
run_tests

# Test with starting value 999,999
solve(999999)